const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const uuidv4 = require('uuid/v4');
const q = require('q');


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});

// const conn = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'user',
//     password : 'password',
//     database : 'sakila'
//   });

//   conn.connect();

//   conn.query('SELECT * from actor', function (err, rows, fields) {
//     if (err) throw err

//     console.log('The solution is: ', rows[0]);
//   })

//   conn.end();


app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});

app.get("/adduser", function (req, res) {
    // console.log(req.query);
    var query = req.query;

    checkUserExists(query.email).then(function (result) {
        // console.log(result);
        if (result) {
            res.status(500).send("User already exists in db");
        } else {
            insertUser(query);
            res.status(200).send("User registration successful");
        }
    }, function (err) {
        res.send(500).send(err);
    });

});

app.get("/actor", function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            console.log(err);
            res.status(500);
            throw err;
        }

        const SQL_QUERY = "SELECT COUNT(*) as n FROM actor";
        conn.query(SQL_QUERY, function (err, result) {
            if (err) {
                conn.release();
                console.log(err);
                res.status(500).send(err);
                throw err;
            }

            console.log(result);
            res.status(200).send(result);
        });

        conn.release();
    });
});

function checkUserExists(email) {

    var deferred = q.defer(); // Use Q 


    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            console.log(err);
            deferred.reject(err);
            throw err;
        }

        const SQL_QUERY = "SELECT COUNT(*) as n FROM users WHERE email='" + email + "'";
        conn.query(SQL_QUERY, function (err, rows, fields) {
            if (err) {
                conn.release();
                console.log(err);
                deferred.reject(err);
                throw err;
            }

            var n = parseInt(rows[0].n);
            deferred.resolve(n > 0);

            conn.release();
        });

    });

    return deferred.promise;
}

function insertUser(user) {
    var uuid = getUUID();
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            console.log(err);
            throw err;
        }

        var SQL_QUERY = "INSERT INTO users (regid, name, email, phone, dob) ";
        SQL_QUERY += " VALUES('" + uuid + "', '" + user.name + "', '" + user.email + "', '" + user.phone + "', '" + formatDate(new Date(user.dob)) + "')";
        conn.query(SQL_QUERY, function (err, result) {
            if (err) {
                conn.release();
                console.log(err);
                throw err;
            }

            // console.log(result);
            conn.release();
        });
    });
}


function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

function getUUID() {
    return uuidv4().substring(0, 8);
}

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app