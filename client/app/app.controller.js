
(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http", "$log"];

    function MyAppCtrl($http) {
        var self = this; // vm
        self.user = {
            name: "",
            email: "",
            phone: "",
            dob: ""
        };

        self.addUser = function () {
            console.log("Registering user...");
            $http.get("/adduser", {
                params: {
                    name: self.user.name,
                    email: self.user.email,
                    phone: self.user.phone,
                    dob: self.user.dob
                }
            })
                .then(function (result) {
                    if (result.status == 200) {
                        alert(result.data);
                    }
                }).catch(function (e) {
                    console.log(e);
                    alert(e.data);
                });
        };

    }

})();