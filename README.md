### connect repo to bitbucket 
1. ```git remote add origin https://kucheech@bitbucket.org/kucheech/iss-fsf-ssf-d12-ex.git```
1. ```git push -u origin master```

### server unit testing
1. ```npm install --save-dev babel-cli babel-preset-env jest supertest superagent```
1. change test in package.json to jest
1. ```npm test```